import cv2
import numpy as np
import os
import shutil
import time
import argparse

start = time.time()

print("Checking arguments ...")
parser = argparse.ArgumentParser()
parser.add_argument("folder", help="relative folder for target")

try:
    args = parser.parse_args()
    p_folder = args.folder

except :
    print("default argument")
    p_folder = "."


matchpath = p_folder + '/match/'

display = 0
debug_output = 0
avoid_rect_x1 = 0
avoid_rect_y1 = 0
avoid_rect_x2 = 1060
avoid_rect_y2 = 125

# check whether directory already exists
if not os.path.exists(matchpath):
  os.mkdir(matchpath)
  print("Folder %s created!" % matchpath)
else:
  print("Folder %s already exists" % matchpath)

totalfile = 0
for file in os.listdir(p_folder):
    if file.endswith(".jpg"):
        # Prints only text file present in My Folder
        print(file)
        totalfile = totalfile + 1

index = 0
for file in os.listdir(p_folder):
    if file.endswith(".jpg"):
        # Prints only text file present in My Folder
        index = index + 1
        infile = p_folder+"/"+file

        img = cv2.imread(infile)
        if(img is not None):
            height, width = img.shape[:2]
            #y_crop = 135 #55 juste le texte, 131 le morceaux de toit et du caca lumineux
            #imgcrop = img[y_crop:height, 0:width]
            img_grey= cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

            #cv2.imshow("Display window", img)
            #k = cv2.waitKey(0) # Wait for a keystroke in the window

            # reading the image in grayscale mode
            #gray = cv2.imread(img, 0)
            
            #detection contour         
            edges = cv2.Canny(img_grey,70,200,apertureSize=3)

            # findcontours
            cnts = cv2.findContours(edges, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)[-2]
            #cnts = cv2.findContours(threshed, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)[-2]
            
            # filter by area
            s1 = 1
            s2 = 10
            xcnts = []
            
            for cnt in cnts:
                if s1<cv2.contourArea(cnt) <s2:
                    xcnts.append(cnt)
                    #x,y,w,h = cv2.boundingRect(cnt)
                    #cv2.rectangle(threshed,(x-5,y-5),(x+w+5,y+h+5),(255,255,255),2)
            nb_dots = 0
            # printing output
            for cnt in xcnts: 
                x,y,w,h = cv2.boundingRect(cnt)
                #cv2.rectangle(threshed,(x-5,y-5),(x+w+5,y+h+5),(255,255,255),2)
                if (
                    ((x>avoid_rect_x1)and(x<avoid_rect_x2)) and
                    ((y>avoid_rect_y1)and(y<avoid_rect_y2))
                   )== False:
                    cv2.circle(img,(x,y), 10, (0,255,0), 2)
                    nb_dots = nb_dots + 1
            
            linesP = cv2.HoughLinesP(edges, 
                                    1, # Distance resolution in pixels
                                    np.pi/180, # Angle resolution in radians
                                    threshold=36, # Min number of votes for valid line
                                    minLineLength=15, # Min allowed length of line
                                    maxLineGap=4 # Max allowed gap between line for joining them
                                    ) #45 10 2
            nb_lines = 0
            if linesP is None:
                nb_lines = 0
            else:
                for i in range(0, len(linesP)):
                            lin = linesP[i][0]
                            if ((
                                ((lin[0]>avoid_rect_x1)and(lin[0]<avoid_rect_x2)) and
                                ((lin[1]>avoid_rect_y1)and(lin[1]<avoid_rect_y2))
                               )or
                               (
                                ((lin[2]>avoid_rect_x1)and(lin[2]<avoid_rect_x2)) and
                                ((lin[3]>avoid_rect_y1)and(lin[3]<avoid_rect_y2))
                               )
                               )== False:
                            
                                cv2.line(img, (lin[0], lin[1]), (lin[2], lin[3]), (0,0,255), 3, cv2.LINE_AA)
                                nb_lines = nb_lines + 1

            if(nb_lines>0):
                shutil.copyfile(infile, matchpath+file)
                newfile = file.replace(".jpg","_processed.jpg")
                newfile_canny = file.replace(".jpg","_canny.jpg")
                #cv2.rectangle(img,(avoid_rect_x1,avoid_rect_y1),(avoid_rect_x2,avoid_rect_y2),(0,255,0),3)
                cv2.imwrite(matchpath+newfile,img)
                cv2.imwrite(matchpath+newfile_canny,edges)
                
            print("Processed "+str(index)+"/"+str(totalfile)+" "+str(file)+" dots: "+str(nb_dots)+" lines: "+str(nb_lines))
            
            if debug_output == 1:
                shutil.copyfile(file, matchpath+file)
                newfile_canny = file.replace(".jpg","_canny.jpg")
                cv2.imwrite(matchpath+newfile_canny,edges)
                
            
            if display == 1:
                scale_percent = 40 # percent of original size
                width = int(img.shape[1] * scale_percent / 100)
                height = int(img.shape[0] * scale_percent / 100)
                dim = (width, height)
                
                # resize image
                img_small = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
                cv2.imshow(file, img_small)
                
                k = cv2.waitKey(0) # Wait for a keystroke in the window
                cv2.destroyWindow(file)
end = time.time()
print("Process time: "+ str(end - start)+"s, "+str((end - start)/totalfile)+"s/img")

